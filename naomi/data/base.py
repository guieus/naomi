from astropy.io import fits
from ..header import get_header, set_header

class Data:
    def __init__(self, data=None, header=None):
        self.set_data(data)
        self.header = dict() if header is None else header
    
    @property
    def data(self):
        return self._data
    
    def set_data(self, data):
        self._data = data
    
    def get_key(self, k):
        return get_header(self.header, k)
    
    def set_key(self, k, value):
        return set_header(self.header, k, value)
    
    @classmethod
    def from_fits(cl, file):
        with fits.open(file) as f:
            data = cl.from_hdulist(f, copy=True)
        return data
    
    @classmethod
    def from_hdulist(cl, hdulist, copy=False):
        if copy:
            data   = hdulist[0].data.copy()
            header = hdulist[0].header.copy()
        else:
            data   = hdulist[0].data
            header = hdulist[0].header
        return cl(data, header)
    
        