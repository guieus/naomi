import numpy as np
pi = np.pi


def cart2pol(x, y):
    rho = np.sqrt(x**2 + y**2)
    phi = np.arctan2(y, x)
    return(rho, phi)

def pol2cart(rho, phi):
    x = rho * np.cos(phi)
    y = rho * np.sin(phi)
    return(x, y)
    

_orient_switch = {
'xy'  : lambda t: t, 
'-xy' : lambda t: -t + pi,
'-x-y': lambda t: t + pi,
'x-y' : lambda t: -t,
'yx'  : lambda t:  t + pi/2, 
'-yx' : lambda t: -t + pi/2,
'y-x' : lambda t: -t + pi + pi/2,
'-y-x': lambda t:  t + pi + pi/2,
}    
def oriented_polar(theta, o, angle=0.0):
    try:
        f = _orient_switch[o]
    except KeyError:
        raise ValueError("Invalid Orientation: %r"%o)    
    return f(theta)+angle

_orient_mg_switch = {
'xy'  : lambda xr,yr,mg: mg(xr,yr),
'-xy' : lambda xr,yr,mg: mg(xr[::-1],yr),
'-x-y': lambda xr,yr,mg: mg(xr[::-1],yr[::-1]),
'x-y' : lambda xr,yr,mg: mg(xr,yr[::-1]),
'yx'  : lambda xr,yr,mg: mg(xr,yr)[::-1],
'-yx' : lambda xr,yr,mg: mg(xr[::-1],yr)[::-1],
'y-x' : lambda xr,yr,mg: mg(xr,yr[::-1])[::-1],
'-y-x': lambda xr,yr,mg: mg(xr[::-1],yr[::-1])[::-1],
}

def oriented_meshgrid(xr, yr, o):
    try:
        f = _orient_mg_switch[o]
    except KeyError:
        raise ValueError("Invalid Orientation: %r"%o)  
    return f(xr,yr, np.meshgrid)
