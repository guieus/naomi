import numpy as np
from collections import OrderedDict
nollIndexes = OrderedDict(zip(range(1,21),[(0,0),(1,1),(1,-1),(2,0),(2,-2),(2,2),(3,-1),(3,1),(3,-3),(3,3), 
                        (4,0),(4,2),(4,-2),(4,4),(4,-4),(5,1),(5,-1),(5,3),(5,-3),(5,5)]))
nollIndexes[20]

ansiIndexes = OrderedDict(zip(range(20), [(0,0), (1,-1), (1,1),(2,-2), (2,0),	(2,2),(3,-3),(3,-1),(3,1), (3,3), 
(4,-4),(4,-2),(4,0),(4,2),(4,4),(5,-5),(5,-3),(5,-1),(5,1),(5,3)]))
ansiIndexes[19]



_zernike_polynomials = [
       ( 13, -13),
       (  0,   0),
       (  1,   1),
       (  1,  -1),
       (  2,   0),
       (  2,  -2),
       (  2,   2),
       (  3,  -1),
       (  3,   1),
       (  3,  -3),
       (  3,   3),
       (  4,   0),
       (  4,   2),
       (  4,  -2),
       (  4,   4),
       (  4,  -4),
       (  5,   1),
       (  5,  -1),
       (  5,   3),
       (  5,  -3),
       (  5,   5),
       (  5,  -5),
       (  6,   0),
       (  6,  -2),
       (  6,   2),
       (  6,  -4),
       (  6,   4),
       (  6,  -6),
       (  6,   6),
       (  7,  -1),
       (  7,   1),
       (  7,  -3),
       (  7,   3),
       (  7,  -5),
       (  7,   5),
       (  7,  -7),
       (  7,   7),
       (  8,   0),
       (  8,   2),
       (  8,  -2),
       (  8,   4),
       (  8,  -4),
       (  8,   6),
       (  8,  -6),
       (  8,   8),
       (  8,  -8),
       (  9,   1),
       (  9,  -1),
       (  9,   3),
       (  9,  -3),
       (  9,   5),
       (  9,  -5),
       (  9,   7),
       (  9,  -7),
       (  9,   9),
       (  9,  -9),
       ( 10,   0),
       ( 10,  -2),
       ( 10,   2),
       ( 10,  -4),
       ( 10,   4),
       ( 10,  -6),
       ( 10,   6),
       ( 10,  -8),
       ( 10,   8),
       ( 10, -10),
       ( 10,  10),
       ( 11,  -1),
       ( 11,   1),
       ( 11,  -3),
       ( 11,   3),
       ( 11,  -5),
       ( 11,   5),
       ( 11,  -7),
       ( 11,   7),
       ( 11,  -9),
       ( 11,   9),
       ( 11, -11),
       ( 11,  11),
       ( 12,   0),
       ( 12,   2),
       ( 12,  -2),
       ( 12,   4),
       ( 12,  -4),
       ( 12,   6),
       ( 12,  -6),
       ( 12,   8),
       ( 12,  -8),
       ( 12,  10),
       ( 12, -10),
       ( 12,  12),
       ( 12, -12),
       ( 13,   1),
       ( 13,  -1),
       ( 13,   3),
       ( 13,  -3),
       ( 13,   5),
       ( 13,  -5),
       ( 13,   7),
       ( 13,  -7),
       ( 13,   9),
       ( 13,  -9),
       ( 13,  11),
       ( 13, -11),
       ( 13,  13),
       ( 13, -13)]

_zernike_numbers = dict(zip(range(len(_zernike_polynomials)), _zernike_polynomials))

_short_names = dict(zip(
    _zernike_polynomials[1:], 
    ['piston',
    'tip','tilt',
    'focus','oastig','vastig',
    'vcoma','hcoma','vtrefoil','otrefoil',
    'spherical','v2astig','o2astig','vquadrafoil','oquadrafoil',
    'h2coma','v2coma','o2trefoil','v2trefoil','opentafoil','vpentafoil']
))

_zernike_polynomials_array = np.array(_zernike_polynomials)

def parse_zernikes(zernikes):
    """ live 2 tuple polynomial as is and transform numbers to polynomials"""
    
    if isinstance(zernikes, np.ndarray):
        dim = len(zernikes.shape)
        if dim==2:
            _, _n = zernikes.shape
            if _n != 2:
                raise ValueError("expecting a Nx2 array got a %r"%zernikes.shape)
            return zernikes
        if dim==1:
            return np.asarray([parse_zernikes(z) for z in zernikes])
        else:
            raise ValueError("expecting a Nx2 array got a %r"%zernikes.shape)    
    
    if isinstance(zernikes, tuple):
        return zernikes
                
    if hasattr(zernikes, "__iter__"):
        return [parse_zernikes(z) for z in zernikes] 
    z = int(zernikes) 
    if z<1:
        raise ValueError('zernike numbers should be >0')
    return _zernike_polynomials[z]


def zernike_number2polynom(zernike_numbers):
    if hasattr(zernike_numbers, "__iter__"):
        return [_zernike_polynomials[z] for z in zernike_numbers]
    else:
        return _zernike_polynomials[int(zernike_numbers)]

def zernike_polynom2number(zernike_polynom):
    if isinstance(zernike_polynom, tuple):
        _zernike_numbers[zernike_polynom]
    if hasattr(zernike_polynom, "__iter__"):
        return [_zernike_numbers[tuple(pol)] for pol in zernike_polynom]
    

def zernike_short_name(zerns):
    zerns = parse_zernikes(zerns)
    if isinstance(zerns, tuple):
        return _short_names[zerns]
    else:
        return [_short_names[z] for z in zerns]


def _noll_to_zern(j):
    """
    Convert from linear Noll index to a tuple of Zernike indicies.
    
    :param int j: Linear Noll Index
    
    """
    if (j < 0):
        raise ValueError("Noll indices start at 0. j={:d}".format(j))
    
    n = np.ceil((-3 + np.sqrt(9 + 8*j))/2)
    m = 2*j - n*(n+2)
    if not int(n) == n:
        raise ValueError("This should never happen, n={:f} should be an integer.".format(n))
    if not int(m) == m:
        raise ValueError("This should never happen, m={:f} should be an integer.".format(m))
    
    return (int(n), int(m))

noll_to_zern = np.vectorize(_noll_to_zern)
noll2zern = noll_to_zern 

def _zern_to_noll(nm):
    """
    Convert a Zernike index pair, (n,m) to a Linear Noll index.
        
    :param n: Zernike `n` index.
    :param m: Zernike `m` index.
    
    """
    n,m = nm
    j = (n * (n+1))/2 + (n+m)/2
    
    if not int(j) == j:
        raise ValueError("This should never happen, j={:f} should be an integer.".format(j))
    
    return int(j)
zern_to_noll = np.vectorize(_zern_to_noll)


def zernfun(na,ma,r,theta,normalized=True):
    """ZERNFUN Zernike functions of order N and frequency M on the unit circle.
   Z = ZERNFUN(N,M,R,THETA) returns the Zernike functions of order N
   and angular frequency M, evaluated at positions (R,THETA) on the
   unit circle.  N is a vector of positive integers (including 0), and
   M is a vector with the same number of elements as N.  Each element
   k of M must be a positive integer, with possible values M(k) = -N(k)
   to +N(k) in steps of 2.  R is a vector of numbers between 0 and 1,
   and THETA is a vector of angles.  R and THETA must have the same
   length.  The output Z is a matrix with one column for every (N,M)
   pair, and one row for every (R,THETA) pair.

   Z = ZERNFUN(N,M,R,THETA,'norm') returns the normalized Zernike
   functions.  The normalization factor sqrt((2-delta(m,0))*(n+1)/pi),
   with delta(m,0) the Kronecker delta, is chosen so that the integral
   of (r * [Znm(r,theta)]^2) over the unit circle (from r=0 to r=1,
   and theta=0 to theta=2*pi) is unity.  For the non-normalized
   polynomials, max(Znm(r=1,theta))=1 for all [n,m].

   The Zernike functions are an orthogonal basis on the unit circle.
   They are used in disciplines such as astronomy, optics, and
   optometry to describe functions on a circular domain.

   The following table lists the first 15 Zernike functions.

       n    m    Zernike function             Normalization
       ----------------------------------------------------
       0    0    1                              1/sqrt(pi)
       1    1    r * cos(theta)                 2/sqrt(pi)
       1   -1    r * sin(theta)                 2/sqrt(pi)
       2    2    r^2 * cos(2*theta)             sqrt(6/pi)
       2    0    (2*r^2 - 1)                    sqrt(3/pi)
       2   -2    r^2 * sin(2*theta)             sqrt(6/pi)
       3    3    r^3 * cos(3*theta)             sqrt(8/pi)
       3    1    (3*r^3 - 2*r) * cos(theta)     sqrt(8/pi)
       3   -1    (3*r^3 - 2*r) * sin(theta)     sqrt(8/pi)
       3   -3    r^3 * sin(3*theta)             sqrt(8/pi)
       4    4    r^4 * cos(4*theta)             sqrt(10/pi)
       4    2    (4*r^4 - 3*r^2) * cos(2*theta) sqrt(10/pi)
       4    0    6*r^4 - 6*r^2 + 1              sqrt(5/pi)
       4   -2    (4*r^4 - 3*r^2) * sin(2*theta) sqrt(10/pi)
       4   -4    r^4 * sin(4*theta)             sqrt(10/pi)
       ----------------------------------------------------

   Example 1:

       % Display the Zernike function Z(n=5,m=1)
       x = -1:0.01:1;
       [X,Y] = meshgrid(x,x);
       [theta,r] = cart2pol(X,Y);
       idx = r<=1;
       z = nan(size(X));
       z(idx) = zernfun(5,1,r(idx),theta(idx));
       figure
       pcolor(x,x,z), shading interp
       axis square, colorbar
       title('Zernike function Z_5^1(r,\theta)')

   Example 2:

       % Display the first 10 Zernike functions
       x = -1:0.01:1;
       [X,Y] = meshgrid(x,x);
       [theta,r] = cart2pol(X,Y);
       idx = r<=1;
       z = nan(size(X));
       n = [0  1  1  2  2  2  3  3  3  3];
       m = [0 -1  1 -2  0  2 -3 -1  1  3];
       Nplot = [4 10 12 16 18 20 22 24 26 28];
       y = zernfun(n,m,r(idx),theta(idx));
       figure('Units','normalized')
       for k = 1:10
           z(idx) = y(:,k);
           subplot(4,7,Nplot(k))
           pcolor(x,x,z), shading interp
           set(gca,'XTick',[],'YTick',[])
           axis square
           title(['Z_{' num2str(n(k)) '}^{' num2str(m(k)) '}'])
       end

   See also ZERNPOL, ZERNFUN2.
   """

    #   Paul Fricker 2/28/2012

    # Check and prepare the inputs:
    # -----------------------------

    na = np.asarray(na).astype(int);
    ma = np.asarray(ma).astype(int);
    r = np.asarray(r);
    theta = np.asarray(theta);

    if na.shape != ma.shape:
        raise "N and M must be the same length";
    
    if not len(na.shape):
        na = np.array([na])
        ma = np.array([ma])
        scalar = True
    else:
        scalar = False
        
        
        

    # if any((na-ma)%2):
    #     raise "All N and M must differ by multiples of 2 (including 0)"
    # 
    # if any(ma>na):
    #     raise "Each M must be less than or equal to its corresponding N."
    # 
    # if any( r>1 | r<0 ):
    #     raise "All R must be between 0 and 1."
    # 
    # if r.shape != theta.shape:
    #     raise "The number of R- and THETA-values must be equal."
        
    rnma = []
    f = np.math.factorial
    z = np.ndarray(na.shape+r.shape)
    for j,(n,m) in enumerate(zip(na, ma)):
        
        m = abs(m)
        rnm = 0.0
        for k in range((n-m)//2+1):
            num = (-1)**k * f(n-k) 
            den = f(k) * f((n+m)//2 -k )*f((n-m)//2-k)
            rnm += num/den * r**(n-2*k)
            
        z[j,...] = rnm
        
        if normalized:            
            z[j] *= np.sqrt((1+(m!=0))*(n+1)/np.pi)
            
    idx_pos = ma>0;
    idx_neg = ma<0;
    if any(idx_pos):
        z[idx_pos,...] *= np.cos(theta*abs(ma[idx_pos]))
    if any(idx_neg):
        z[idx_neg,...] *= np.sin(theta*abs(ma[idx_neg]))
    if scalar:
        return z[0,...]
    else:
        return z
    
def cart2pol(x, y):
    rho = np.sqrt(x**2 + y**2)
    phi = np.arctan2(y, x)
    return(rho, phi)

def pol2cart(rho, phi):
    x = rho * np.cos(phi)
    y = rho * np.sin(phi)
    return(x, y)


if __name__ == "__main__":
    from matplotlib.pylab import plt
    X,Y = np.meshgrid(np.linspace(-1,1, 128), np.linspace(-1,1, 128))
    r, t = cart2pol(X,Y)
    
    plt.subplot(4,4,1)
    z = zernfun(0,0, r, t)
    z[r>1] = 0.0;
    plt.imshow(z)
    plt.subplot(4,4,5)
    z = zernfun([1],[-1], r, t)[0]
    z[r>1] = 0.0;
    plt.title("%.3f"%np.std(z[r<1].flat))
    plt.imshow(z)
    plt.subplot(4,4,6)
    z = zernfun([1],[1], r, t)[0]
    z[r>1] = 0.0;
    plt.title("%.3f"%np.std(z[r<1].flat))
    plt.imshow(z)
    plt.subplot(4,4,9)
    z = zernfun([2],[-2], r, t)[0]
    z[r>1] = 0.0;
    plt.title("%.3f"%np.std(z[r<1].flat))
    plt.imshow(z)
    plt.subplot(4,4,10)
    z = zernfun([2],[0], r, t)[0]
    z[r>1] = 0.0;
    plt.title("%.3f"%np.std(z[r<1].flat))
    plt.imshow(z)
    plt.subplot(4,4,11)
    z = zernfun(2,2, r, t, normalized=True)
    z[r>1] = 0.0;
    plt.title("%.3f"%np.std(z[r<1].flat))
    plt.imshow(z)
    
    plt.imshow(z)
        
    plt.show();
    
    
