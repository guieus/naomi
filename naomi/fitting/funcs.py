import numpy as np 
def gauss(xy, *p):
    """ p is the gaussian parameters (4)  [amplitude, x0, y0, fwhm] """
    return  p[0]*np.exp( -( (xy[0]-p[1])**2 + (xy[1]-p[2])**2)/(2*p[3]**2) );

def gaussE(xy,*p):
    """ p is the gaussian parameters (4)  [amplitude, x0, xFwhm, y0, yFwhm]"""
    return  p[0]*np.exp( -( (xy[0]-p[1])**2/(2*p[2]**2) + (xy[1]-p[3])**2)/(2*p[4]**2) );

def gaussER(xy,*p):
    """ p is the gaussian parameters (6)  [amplitude, x0, xFwhm,  y0, yFwhm, rotAngle]"""
    datarotx= xy[0]*np.cos(p[5]) - xy[1]*np.sin(p[5]);
    dataroty= xy[0]*np.sin(p[5]) + xy[1]*np.cos(p[5]);
    x0rot = p[1]*np.cos(p[5]) - p[3]*np.sin(p[5]);
    y0rot = p[1]*np.sin(p[5]) + p[3]*np.cos(p[5]);
 
    return p[0]*np.exp(-((datarotx-x0rot)**2/(2*p[2]**2) + (dataroty-y0rot)**2/(2*p[4]**2)));

def lorentz(xy,*p):
    """ 2d simetrical lorentzian
        p is the lorentz parameters (5)  [amplitude, x0, y0, xHwhm]
    """
        
    return  p[0] / ((xy[0] - p[1])**2 / (p[3])**2 + (xy[1] - p[2])**2 / (p[3])**2 + 1)

def lorentzE(xy,*p):    
    """ 2d lorentzian
        p is the lorentz parameters (5)  [amplitude, x0, xHwhm,  y0, yHwhm]
    """
    return  p[0] / ((xy[0] - p[1])**2 / (p[2])**2 + (xy[1] - p[3])**2 / (p[4])**2 + 1)
    
def naomi(xy,*p):
    """ 2d circular gaussian ^3/4
        p is the gaussian parameters (5)  [amplitude, x0, y0, fwhm]
    """    
    #return  p[0]*np.exp( -(( (xy[0]-p[1])**2 + (xy[1]-p[2])**2)/(2*p[3]**2)) )
    return  p[0]*np.exp( -(( (xy[0]-p[1])**2 + (xy[1]-p[2])**2)/(2*p[3]**2))**0.75 )


def maximum(xy,*p):
    """ just a circle arround maximum found p is [amplitude, x0, y0, hwhm]"""
    return p[0]*(   ( (xy[0]-p[0])**2 + (xy[1]-p[1])**2 )<p[2]**2)

