class Config:
    pupil = None
    dm = None
    alignment = None
    cm = None    
    def __init__(self, **kwargs):
        
        # everything defined which is not a method and does not 
        # start with '_' is a default and need to go inside 
        # self.__dict__
        # for m in self.__class__.__mro__:
        #     for k,v in m.__dict__.items():
        #         if not k.startswith('_') and\
        #             v is not None and\
        #             not hasattr(v, "__call__"):
        #             kwargs.setdefault(k,v)
        
        self.__dict__.update(kwargs)    
        
        for m in self.__class__.__mro__:
            for k,v in m.__dict__.items():
                if not k.startswith('_') and\
                    v is not None and\
                    not hasattr(v, "__call__") and\
                    not hasattr(v, "__get__") and\
                    k  not in self.__dict__:
                        if not isinstance(v, Config):
                            self.__dict__[k] = v
    
    # def __getattr__(self, attr):
    #     try:
    #         return self.__dict__[attr]
    #     except KeyError:
    #         return TmpConfig(self.__dict__, (attr,))
    
    def __getitem__(self, item):
        hasdefault = False
        if isinstance(item, tuple):
            item, default = item
            hasdefault = True
        if not isinstance(item, str):
            raise ValueError('item key must be a string')        
        items = item.split('.')
        
        out = self
        
        for part in items:
            try:
                out = getattr(out, part)
            except AttributeError:
                if hasdefault:
                    return default                
                else:
                    raise KeyError(item)                    
        return out
    
    def keys(self):
        for k,v in self.__dict__.items():
            if not isinstance(v, Config):
                if not k.startswith("_"):
                    yield k  
    
    def copy(self, **kwargs):
        return self.__class__(**dict(self.__dict__, **kwargs))
    
    def __repr__(self):
        tab = "  "
        indent = ""
        depth = 0
        txt = ""
        
        if len(self.__dict__):
            l = min([max(len(k) for k in self.__dict__.keys() if k[0]!="_"), 12])
        else:
            l = 12
            
        fk = "%s"  
        fkv =  "%%-%ds : %%r"%l
        
        for k,v in self.__dict__.items():
            if k[0]!="_":
                if isinstance(v, Config):
                    txt += (fk%k)+"\n"
                    txt += _shorten_lines(_tabify("%r"%v, tab)+"\n", 60)
                    #txt += ("%r"%v)+"\n"
                else:
                    txt += _shorten_str(fkv%(k, v), 80)+"\n"
        
        # for m in self.__class__.__mro__:
        #     for k,v in m.__dict__.items():
        #         if not k.startswith('_') and\
        #             v is not None and\
        #             not hasattr(v, "__call__") and\
        #             not hasattr(v, "__get__") and\
        #             k  not in self.__dict__:
        #                 if isinstance(v, Config):
        #                     txt += (fk%k)+"\n"
        #                     txt += _shorten_lines(_tabify("%r"%v, tab)+"\n", 60)
        #                     #txt += ("%r"%v)+"\n"
        #                 else:
        #                     txt += _shorten_str(fkv%(k, v), 80)+"\n"                            
        return txt
        
        
def _shorten_str(s,maxlen):
    return s[:maxlen]+"..." if len(s)>maxlen else s

def _shorten_lines(s, maxlen):
    return "\n".join((l.rstrip()[:maxlen]+"..." if len(l.rstrip())>maxlen else l) for l in s.split("\n"))

def _tabify(s, indent):
    lines = s.split("\n")
    return indent+("\n"+indent).join(lines)


class TmpConfig:
    _master = None
    _path = None
    def __init__(self, master, path):
        self._master = master
        self._path = path
    
    def __build__(self):
        d = self._master
        for p in self._path:
            d = d.setdefault(p, Config()) 
        return d    
    
    
    def __setitem__(self, key, value):
        if key in ("_master", "_path"):
            self.__dict__[key] = value
        else:
            d = self.__build__()
            d[key] = value                
        
    
    def __getitem__(self, key):
        d = self._master
        for p in self._path:
            try:
                d = d[p]
            except KeyError:
                raise KeyError(key)            
        return d[key]    
    
    def __getattr__(self, attr):
        try:
            return self.__getitem__(attr)
        except KeyError:
            return TmpConfig(self._master, self._path+(attr,))
    
    def __setattr__(self, attr, value):
        self.__setitem__(attr, value)
    
# class RegisterLeafClasses(type):
#     def __init__(cls, name, bases, nmspc):
#         super(RegisterLeafClasses, cls).__init__(name, bases, nmspc)
#         if not hasattr(cls, 'registry'):
#             cls.registry = set()
#         cls.registry.add(cls)
#         cls.registry -= set(bases) # Remove base classes
#     # Metamethods, called on class objects:
#     def __iter__(cls):
#         return iter(cls.registry)
#     def __str__(cls):
#         if cls in cls.registry:
#             return cls.__name__
#         return cls.__name__ + ": " + ", ".join([sc.__name__ for sc in cls])

class Register:    
    default = None
    def __init__(self, name, default=None):
        self.name = name
        self.default = default
        self.class_register = {}
        self.obj_register = {}
        
    def register_class(self, name, cls):
        self.class_register[name] = cls
        
    def register(self, name, obj):
        self.obj_register[name] = obj
        
    def get_class(self, name):
        return self.class_register[name]
    
    def get(self, name):
        return self.obj_register[name]
    
    def set_default(self, name_obj):
        obj = self.parse(name_obj)
        self.default = obj
            
    def parse(self, name_obj):
        if name_obj is None:
            if self.default is None:
                raise ValueError('%s registery has no default'%self.name)
            return self.default        
        if isinstance(name_obj, str):
            return self.get(name_obj)
        return name_obj
    
      
    
    
        
    
    
    
    
    
    
    
    
    