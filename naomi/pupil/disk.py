from .base import Pupil, PupilInstance, pupil_register
from ..config.alignment import Alignment
from ..header import get_header, set_header
import numpy as np

class Disk(Pupil):
    type = 'DISK'
    def __init__(self, diameter, unit='pix'):
        Pupil.__init__(self, diameter=diameter, unit=unit)
        
        
    def make(self, dim, center, scale=None, angle=0.0):        
        if self.unit == 'pix':
            r = self.diameter / 2.0
        else:
            if scale is None:
                raise ValueError("for unit different than 'pix', a scale must be provided")
            r = self.diameter / np.mean(scale) / 2.0
            
        x,y = np.meshgrid(range(dim[1]), range(dim[0]));
        mask = (x-center[0])**2 + (y-center[1])**2 < r**2
                
        return PupilInstance(mask, np.array([r*2, r*2]), center, angle=angle)
        
    def make_from_alignment(self, alignment, wfs):
        return self.make(wfs.size, alignment.center, alignment.scale)
    
    def make_from_ifm(self, ifm, dm=None):
        alignment = Alignment.from_ifm(ifm, dm)                
        N, Ny, Nx = ifm.shape
        return self.make((Ny,Nx), alignment.center, alignment.scale, angle=alignment.angle)
    
    def make_from_header(self, h):
        nsub = int(get_header(h, 'WFS.NSUBPUPIL'))
        alignment = Alignment.from_header(h)
        return self.make((nsub,nsub), alignment.center, alignment.scale, angle=alignment.angle)
    
    @classmethod
    def from_header(cl, h, suffix="", __fmh__=False):                
        diameter = get_header(h, suffix+"PUPIL.DIAMETER")
        unit = get_header(h, suffix+"PUPIL.DIAMETER")
        return cl(diameter, unit)
                        
pupil_register.register_class('DISK', Disk)

    #      (1:nSubAperture,1:nSubAperture);
    #Z = (X-xCenterPix).^2 + (Y-yCenterPix).^2 < (diameterPix*diameterPix/4);                
            
            
        
        
    
        
        