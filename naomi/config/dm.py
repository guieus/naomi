from ..misc.mesh import oriented_meshgrid
from .base import Config, Register
from ..header import get_header, set_header 

dm_register = Register('dm')

class DM(Config):
    actuator_separation = (0.0, 0.0)
    x = None
    y = None
    i = None
    j = None
    mask = None
    centralact = 0
    model = ""
    # def __init__(self, orientation='xy'):
    #     pass
    
    @staticmethod
    def from_header(h):
        try:
            dmtype = get_header(h, 'DM.TYPE')
        except KeyError:
            raise ValueError('Cannot found DM.TYPE information in header')
                                
        return get_dm_class(dmtype)(get_header(h, 'DM.ORIENTATION', 'xy'))


class DM241(DM):
    """ Object use to represent the DM241 of Alpao 
        
    Properties
    ----------
    mask : (17,17) boolean array 
          sum(mask) == 241  the position of the actuator in a 17x17 grid 
    i : (241,) int array 
        column number of the actuator from -8 to 8 
    j : (241,) int array 
        row number of the actuator from -8 to 8
    x : (241,) float 
        physical x position of the actuators (0.0 in the middle)
    y : (241,) float 
        physical y position of the actuators (0.0 in the middle)
    orientation : string 
        Change the coordinates above for graphical orientation
        'xy' is the default 
        '-xy' is a flip on x axis 
        'yx'  rotation of 90degree
        '-yx' rotation of 90 degree and flip on yaxis
        etc ...  
        
    Inputs 
    ------
    orientation :  strin, optional 
            
    """
    actuator_separation = (2.5e-3, 2.5e-3) # in m
    centralact = 120
    model = "DM241"
    def __init__(self, orientation='xy', **kwargs):
        Config.__init__(self, **kwargs)
        # make a copy        
        self.actuator_separation = tuple(self.actuator_separation)
        self.orientation = orientation
        self.model = self.model 
        
        X, Y = oriented_meshgrid(range(-8,9),range(-8,9),orientation)        
        self.mask = (Y**2+X**2) < (8.75*8.75);        
        #  only actuator in mask
        self.i = X[self.mask];
        self.j = Y[self.mask];
        self.x = self.i * self.actuator_separation[0]
        self.y = self.j * self.actuator_separation[1]
        
    def to_header(self,h=None):
        h = dict() if h is None else h
        set_header(h, 'DM.CENTRALACT', self.centralact)
        set_header(h, 'DM.ACT_SEPARATION', self.actuator_separation)
        set_header(h, 'DM.NACT', len(self.i))
        set_header(h, 'DM.ORIENTATION', self.orientation)
        return h
        
dm_register.register_class('DM241', DM241) 




