from .base import Data
import numpy as np
from ..misc.parser import asdm
from ..compute.command import (if_center, ifm_scale, command_matrices, 
                              clean_if, nanrms, zernike_vector, theoritical_phase, 
                              theoritical_phase
                              )  
from ..compute.phase import zernike_number2polynom                              
from ..fitting import fit_if_profile, if_profile, img_if_profile

from ..pupil.base import Pupil, PupilInstance
from ..misc.parser import asarray

def _wr_modes(h, zernikes):
    zernikes = parse_zernikes(zernikes)
    if isinstance(zernikes, tuple):
        zernikes = [zernikes]
        
    h['NMODE'] = len(zernikes)
    for i,(n,m) in enumerate(zernikes):
        h['MODE%d'%i] = "%d,%d"%(n,m)
    
def _rd_modes(h):
    try:
        n = h['NMODE']
    except KeyError:
        return []
    else:
        return [tuple(int(x) for x in h['MODE%d'%i].split(",")) for i in range(n)]    
    
class PhaseScreen(Data):
    """ Data object representing a Phase Screen """
    
    def rms(self, mask=None):
        """ phase screen rms """
        if mask:
            return nanrms(self.data[mask])
        else:
            return nanrms(self.data)
    def ptv(self, mask=None):
        """ phase screen Pick to Valley """
        if mask:
            data = self.data[mask]
        else:
            data = self.data
        
        return np.nanmax(data)-np.nanmin(data)        

class InfluenceFunction(PhaseScreen):
    """ Data object representing a pure Influence Function Phase Screen """
    def __init__(self, *args, **kwargs):
        super(InfluenceFunction, self).__init__(*args, **kwargs)
        self._profile_cash = {}
    
    @property
    def actuator(self):
        return self.header.get('ACTUATOR', -999)
    
    @property
    def amplitude(self):
        return self.header['IF AMPLITUDE']
    
    def set_data(self, data):
        super(InfluenceFunction, self).set_data(data)
        self._profile_cash = {}
    
        
    def profile(self, ftype='naomi', sigma_guess=10, method='dogbox'):
        pc = self._profile_cash
        try:
            return pc[(ftype, sigma_guess, method)]
        except KeyError:
            res = fit_if_profile(self.data, ftype, sigma_guess=sigma_guess, method=method)
            pc[(ftype, sigma_guess, method)] = res
            return res
    
    def profile_model(self, fitresult=None):
        if fitresult is None:
            fitresult = self.profile()
        model = img_if_profile(self.data.shape, fitresult)
        return model
    
    def residual(self, fitresult=None):
        model = self.profile_model(fitresult)        
        return self.data - model
    
    def cleaned(self, rad, prc=50, cleanTipTilt=False):
        data = clean_if(self.data, rad, prc, cleanTipTilt=cleanTipTilt)
        header = self.header.copy()
        cleaned = self.__class__(data, header)
        
        cleaned.set_key('IF CL PERCENTIL', prc)
        cleaned.set_key('IF CL RADIUS', rad)
        cleaned.set_key('IF CL TIPTILT', cleanTipTilt)
        cleaned.set_key('IF CLEANED', True)
        return cleaned
    
    def plot_profile_cut(self, ax, axis='y', color='k'):
        res = self.profile()
        model = img_if_profile(self.data.shape, res)
        if axis == "y":
            ax.plot(self.data[int(res.center[1]),:], color=color)
            ax.plot(model[int(res.center[1]),:], linestyle=":", color=color)
        elif axis == "x":
            ax.plot(self.data[:,int(res.center[0])], color=color)
            ax.plot(model[:,int(res.center[0])], linestyle=":", color=color)
        else:
            raise ValueError('axis should be "x" or "y" got %r'%axis)
    
class ModePhaseScreen(PhaseScreen):
    """ Data object representing a pure Mode Phase Screen """
    @property
    def zernike(self):
        return self.header['ZERNIKE']
    
    @property
    def amplitude(self):
        return self.header['MODE.AMPLITUDE']
    
    def zernike_vector(self,ptz):
        zv_array = zernike_vector(self.data, ptz)
        h = {}        
        return zv_array
    
    
    @classmethod
    def from_pupil(self, zernike, pupil, orientation='xy', angle=0.0):
        phase = theoritical_phase(zernike_number2polynom([zernike]), pupil, orientation=orientation, angle=angle)[0]
        h = {'ZERNIKE':zernike, 'MODE AMPLITUDE':1.0, 'TEHORITICAL':True}
        return self.__class__(phase, h)
    
    @classmethod
    def from_ztp(self, zernike, ztp):
        if isinstance(ztp, np.ndarray):
            if len(ztp.shape)!=3:
                raise ValueError('Expecting a 3 dimentional array')
        elif isinstance(ztp, ztp):
            return ztp.mode(zernike)
        else:
            ValueError('expecting a 3d ndarray as ztp')
                
        phase = ztp[zernike-1]        
        h = {'ZERNIKE':zernike}
        
        return self.__class__(phase, h)
    
    def theoritical_mode(self, ztp_or_pupil=None):            
        z = self.zernike            
        if isinstance(ztp_or_pupil, (Pupil, PupilInstance)):
            if isinstance(ztp_or_pupil, Pupil):
                pupil = ztp_or_pupil.make_from_header(self.header)
            else:
                pupil = ztp_or_pupil
            orientation = self.get_header('ALIGNMENT ORIENTATION')
            angle = self.get_header('ALIGNMENT ANGLE')    
            return self.from_pupil(z,  pupil, orientation=orientation, angle=angle)
        elif isinstance(ztp_or_pupil, np.ndarray):
            return self.from_ztp(z, ztp_or_pupil)                                                          
        else:
            raise ValueError('Either a ztp array or a Pupil object or PupilIntance')



### Data for array of phase

class PhaseScreens(Data):
    """ Cube of phase screen """
    def zernike_vector(self, ptz):
        """ zernike vector """
        return np.array([zernike_vector(p, ptz) for p in self])
    
    def rms(self, mask=None):
        if mask:
            return np.array([nanrms(p[mask]) for p in self])
        else:
            return np.array([nanrms(p) for p in self])
    
    def phase_screen(self, index):
        """ return a PhaseScreen object of the given index """
        data  = self.data[index]
        return PhaseScreen(data, {})    

class ModePhaseScreens(PhaseScreens):    
    def phase_screen(self, zernike):
        return ModePhaseScreen(self.data[zernike-1], {'ZERNIKE':zernike})
    

    
    
        