

header_keys = {
'ALIGNMENT.XCENTER':
    {'altKey':['ALIGNMENT XCENTER', 'XCENTER'], 
    'comment':'[pixel] Center of the Pupil in the x direction', 
    'unit' : 'pixel', 'fmt': '%.3f' 
    }, 
'ALIGNMENT.YCENTER':
    {'altKey':['ALIGNMENT YCENTER','YCENTER'], 
    'comment':'[pixel] Center of the Pupil in the y direction', 
    'unit' : 'pixel', 'fmt': '%.3f'
    }, 
'ALIGNMENT.XPSCALE':
    {'altKey':['ALIGNMENT XPSCALE', 'XPSCALE'], 
    'comment':'[m/pixel] wfs to Pupil scale in the x direction',
    'unit' : 'm/pixel', 'fmt': '%.3e'
    }, 
'ALIGNMENT.YPSCALE':
    {'altKey':['ALIGNMENT YPSCALE', 'YPSCALE'], 
    'comment':'[m/pixel] wfs to  Pupil scale in the y direction',
    'unit' : 'm/pixel', 'fmt': '%.3e'
    }, 
'ALIGNMENT.ANGLE':
    {'altKey':['ALIGNMENT ANGLE', 'DMANGLE'], 'comment':'[rad] dm to wfs angle', 
    'unit' : 'rad', 'fmt': '%.5f'
    }, 
'DM.ACT_SEPARATION':
    {'altKey':['DM ACT_SEPARATION', 'ACTSEP'], 'comment':'[m]separation between actuators', 
    'unit':'m', 'fmt': '%.3e'
    },
'DM.CENTRALACT':
    {'altKey':['DM CENTRALACT', 'CENTACT'], 'comment':'[#] Central actuator number', 
    'unit':'', 'fmt': '%d'
    },
'DM.NACT':
    {'altKey':['DM NACT','DM_NACT'], 'comment':'[#] Number of actuators', 
    'unit':'', 'fmt': '%d'
    },
'DM.ORIENTATION':
    {'altKey':['DM ORIENTATION', 'DMORIENT'], 'comment':'[#] graph DM orientation', 
    'unit':'', 'fmt': '%s'
    },
'WFS.NSUBAPERTURE':
    {'altKey':['WFS NSUBPupil', 'NSUB'], 'comment':'[#] number of subPupil', 
    'unit':'', 'fmt': '%d'
    },
'WFS.NSUBPupil':
    {'altKey':['WFS NSUBAPERTURE', 'NSUB'], 'comment':'[#] number of subPupil', 
    'unit':'', 'fmt': '%d'
    },
}

class NULL:
    pass

def undot(s):
    return " ".join(s.split('.'))
    
def read_header(hin):
    hout = {}
    for k in header_keys:
        try:
            v = get_header(hin,k)    
        except KeyError:
            try:
                v = get_header(hin,undot(k))
            except KeyError:
                continue 
        else:
            hout[k] = v
    return hout

def get_header(h, k, default=NULL):        
    try:
        return h[k]
    except KeyError:
        try:
            return h[undot(k)]
        except KeyError:
            pass
        
        try:
            d = header_keys[k]
        except KeyError:
            if default is not NULL:
                return default 
            else:
                raise KeyError(k)
        else:        
            for k in d.get('altKey', []):
                try:
                    return h[k]
                except KeyError:
                    pass
    
    if default is not NULL:
        return default                
    allk =  "', '".join([k]+d.get('altKey', []))
    raise KeyError("cannot found any of '%s' keys", allk)
    
def set_header(h, k, v):
    h[k] = v

