# What it is ? 

The module naomi was first created to calibrate the ALPAO DM of naomi. However it has buitl to be the most generalist as possible and can server for other purpose. 

The main goal is to be able to compute a Modal Command Matrix for the DM from an Influance Function matrix  

# Installation 

```
>>> git clone git@gricad-gitlab.univ-grenoble-alpes.fr:guieus/naomi.git
>>> cd naomi
>>> python setup.py install
```

or 

```
>>> pip install naomi 
```

# Doc 

To be Done but this notebook give great examples:

[notebook example](https://gricad-gitlab.univ-grenoble-alpes.fr/guieus/naomi/-/blob/master/notebooks/naomi%20example%20and%20doc.ipynb) 

Please visit the code as well :) 

