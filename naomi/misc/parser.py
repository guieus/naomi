from .registers import pupil_register
from ..config.dm import dm_register
import numpy as np

def asarray(obj):
    return np.asarray(obj)

def aspupil(pupil):
    """ a pupil object parser 
    
    If pupil is a string look inside the registered pupil to retrive the
    pupil. Otherwise assume the input is a pupil object 
    """
    return pupil_register.parse(pupil)

def asdm(dm):
    """ a DM object parser 
    
    if dm is a string look inside the dm_register and return the matching dm
    otherwise assume this is a DM object
    """
    return dm_register.parse(dm)