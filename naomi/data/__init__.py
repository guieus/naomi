from .base import *
import os
import numpy as np 

import pkg_resources
from astropy.io import fits

def load_ifm_example_dm241():
    ifm_file = pkg_resources.resource_filename('naomi', os.path.join('resources', 'example_ifm.npz'))
    with np.load(ifm_file) as data:
        ifm = data['ifm']
    return ifm 
