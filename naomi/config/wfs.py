from .base import Config, Register
from ..header import get_header, set_header
import numpy as np


wfs_register = Register('wfs')
class Wfs(Config):
    size = (0,0)
    model = ""    
    
    @staticmethod
    def from_header(h):
        try:
            model = get_header(h, 'WFS.MODEL')
        except  KeyError:
            raise ValueError('Cannot found WFS model in header')
        
        serial = get_header(h, 'WFS.SERIAL', '')
        
        return wfs_register.get_class(model)(serial = serial)
        
    def to_header(self, h=None):
        h = dict() if h is None else h
        set_header(h, 'WFS.MODEL', self.model)
        set_header(h, 'WFS.NSUBAPERTURE', int(np.mean(self.size)))        
        try:
            set_header(h, 'WFS.SERIAL', self.serial)
        except AttributeError:
            pass                    
    
class HASO128(Wfs):    
    size = (128,128)
    model = 'haso128'

wfs_register.register_class('haso128', HASO128)    
wfs_register.register('haso128', HASO128())   
