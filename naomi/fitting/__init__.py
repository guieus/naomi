from . import funcs
from ..compute.command import if_center 
from scipy.optimize import curve_fit
import numpy as np 

class FitResult:
    """ created only by fit_if_profile
    
    it contains fits results, as :
        .type 
        .amplitude 
        .center
        .hwhm 
        .angle 
        .offset
        .func
        .err 
    
    and methods : 
        .model(xy) -> model from a (2x...) array of xy pixel coordinate
        .img_model(shape) -> make an image of the given shape 
    
    """
    def model(self, xy):
        return if_profile(xy, self)
    def img_model(self, shape):
        return img_if_profile(shape, self)        
    
    
class FitResultList(list):
    @property
    def center(self):
        return np.array([r.center for r in self])
    @property
    def hwhm(self):
        return np.array([r.hwhm for r in self])
    @property
    def angle(self):
        return np.array([r.angle for r in self])
    @property
    def amplitude(self):
        return np.array([r.amplitude for r in self])
    

def fit_ifm_profile(ifm, *args, **kwargs):
    """ Fit IMF profiles 
    
    Inputs
    ------
    ifm : (Nact, Nx, Ny) array 
    fit_type: str
        one of gauss, gaussE, gaussER, lorentz, lorentzE, maximum or naomi
    
    sigma_guess : float, 
        sigma starting point for the fit
    amplitude_guess : float
        amplitude starting point 
    method : fit method  for scipy.optimize.curve_fit function, default is dogbox
    
    Outputs
    -------
    lst : FitResultList a list of FitResult object with some usefull propoerty as 'center', 'hwhm', 'angle','amoplitude'
    """
    return FitResultList([fit_if_profile(IF,*args, **kwargs) for IF in ifm])    

def fit_if_profile(IF, fit_type='naomi', sigma_guess=10, amplitude_guess=7, method='dogbox'):
    """ Fit one IF profile
    
    Inputs
    ------
    if : (Nx, Ny) array Influence function phase screen 
    fit_type: str
        one of gauss, gaussE, gaussER, lorentz, lorentzE, maximum or naomi
    
    sigma_guess : float, 
        sigma starting point for the fit
    amplitude_guess : float
        amplitude starting point 
    method : fit method  for scipy.optimize.curve_fit function, default is dogbox
    
    Outputs
    -------
    result : FitResult object containing fits results 
         result.type 
         result.amplitude 
         result.center
         result.hwhm 
         result.angle 
         result.offset
         result.func
         result.err 
        methods : result.model(xy) -> model from a (2x...) array of xy pixel coordinate
                  result.img_model(shape) -> make an image of the given shape 
    
    """
    #% guess the center by barycenter for initial value 
    #% this is necessary for the fit to comverge properly 
    xC, yC = if_center(IF)
    ySize, xSize = IF.shape
    
    if fit_type is 'maximum':
        s = np.sign(IF[np.min(np.max(int(yC),1),ySize), np.min(np.max(int(xC),1), xSize)])
        
        b = 4
        #% take the maximum of a small box within the xC, yC
        boxed = IF[ np.max(int(yC-b),1):np.min(int(yC+b),ySize),
                    np.max(int(xC-b),1):np.min(int(xC+b),ySize)]
                    
        if s>0:
            amplitude = np.max(boxed)
        else:
            amplitude = np.min(boxed)
        
        # compute the sum of what is superior to maximum half
        num = np.nansum(IF.flat > (amplitude * 0.5))
        # and deduct the half width of maximum
        hwhm= np.sqrt(num/np.pi)
            
        result = FitResult()
        result.type = 'maximum'
        result.amplitude = amplitude
        result.center = (xC,yC)
        result.hwhm = (hwhm, hwhm)
        result.angle = 0.0
        result.offset = 0.0
        result.func = funcs.maximum
        return result
    
    X,Y = np.meshgrid(range(xSize), range(ySize))
 	
    mask = ~np.isnan(IF)
    
    Z = IF[mask]
    X = X[mask]
    Y = Y[mask]
    
    fluxTreshold = 0.1
    aZ = np.abs(Z)
    t =  np.max(aZ)* fluxTreshold
    fluxMask =  (aZ > t)
    
    
    
    #% limits the image to points around xC, yC 
    maxRadius = 30
    maskIn = (X-xC)**2 +  (Y-yC)**2 <= maxRadius**2
       
    # take only the points with enough flux or within a box around the max
    # point
    mask =  fluxMask | maskIn
    
    # compute the offset background from everything
    # that is not inside the radius 
    offset = np.median(Z[~mask])
    Z = Z[maskIn]
    X = X[maskIn]
    Y = Y[maskIn]
    
    
    xy = [X,Y]
    # substract the offset to Z
    # the offset value is stored in fitResult
    # the offset is then added to model with the function if_profile
    Z = Z-offset
    
    amax = 1e40
    amin = 0.0
    smin = 2.0
        
    if fit_type == 'gauss':
        args0 = [amplitude_guess,xC, sigma_guess, yC]
        lb = [amin, 1, smin, 1 ]
        ub = [amax, xSize,xSize,ySize]
        func = funcs.gauss
    elif fit_type == 'naomi':    
        args0 = [amplitude_guess,xC, sigma_guess, yC]
        lb = [amin, 1, smin, 1 ]
        ub = [amax, xSize,xSize,ySize]
        func = funcs.naomi
    elif fit_type == 'gaussER':
        args0 = [amplitude_guess,xC, sigma_guess, yC, sigma_guess, 0.0]
        # define lower and upper bounds [Amp,xo,wx,yo,wy, phi]
        lb = [amin, 1, smin, 1, smin, -np.pi/4]
        ub = [amax ,xSize,xSize,ySize,ySize,np.pi/4]
        func = funcs.gaussER
    elif fit_type == 'gaussE':         
        args0 = [amplitude_guess,xC, sigma_guess, yC, sigma_guess]
        lb = [amin, 1, smin, 1, smin]
        ub = [amax, xSize,xSize,ySize,ySize]
        func = funcs.gaussE
    elif fit_type == 'lorentzE':              
        args0 = [amplitude_guess,xC, sigma_guess, yC, sigma_guess]
        lb = [amin, 1, smin, 1, smin]
        ub = [amax, xSize,xSize,ySize,ySize]
        func = funcs.lorentzE
    elif fit_type == 'lorentz': 
        args0 = [amplitude_guess, xC, yC, sigma_guess]
        lb = [amin, 0.0  , 0.0 , smin]
        ub = [amax, xSize,xSize, 40]
        func = funcs.lorentz
    else:
        raise ValueError('Fit Type must be gauss, gaussE, gaussER, lorentz, lorentzE, maximum or naomi')
    
    
    args, pcov = curve_fit(func,xy,Z,args0, bounds=(lb,ub), method=method)
    
    err = np.sqrt(np.diag(pcov))
    
    result = FitResult()
    result.type = fit_type
    result.func = func
    result.err = err    
    result.offset = offset
    
    args2result(args, result) 
    return result

def args2result(args, result):
    fit_type = result.type
    result.amplitude = args[0]
    if fit_type in ['gauss', 'naomi', 'lorentz', 'maximum']:
        result.center = (args[1],args[2])                
        result.hwhm = (args[3], args[3])
        result.angle = 0.0
    if fit_type in ['gaussE','lorentzE']:
        result.center = (args[1],args[3])
        result.hwhm = (args[2], args[4])
        result.angle = 0.0
    if fit_type is 'gaussER':
        result.center = (args[1],args[3])
        result.hwhm = (args[2], args[4])
        result.angle = args[5]
            
def result2args(r):
    fit_type = r.type
    if fit_type in ['gauss', 'naomi', 'lorentz', 'maximum']:
        return (r.amplitude,)+tuple(r.center)+(r.hwhm[0],)
    if fit_type in ['gaussE', 'lorentzE']:
        return (r.amplitude,)+(r.center[0], r.hwhm[0], r.center[1], r.hwhm[1])
    if fit_type is 'gaussER':
        return (r.amplitude,)+(r.center[0], r.hwhm[0], r.center[1], r.hwhm[1], r.angle)
    if fit_type is 'lorentzE':
        return (r.amplitude,)+(r.center[0], r.hwhm[0], r.center[1], r.hwhm[1])
    raise ValueError('BUG!')

def img_if_profile(shape, fit_result):
    xy = np.meshgrid(range(shape[0]), range(shape[1]))
    return if_profile(xy, fit_result)

def if_profile(xy, fit_result):    
    return fit_result.func(xy, *result2args(fit_result))+fit_result.offset

    
    
    