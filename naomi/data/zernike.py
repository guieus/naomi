from .base import Data
from ..compute.command import theoritical_phase
from ..compute.phase import zernike_number2polynom,zernike_polynom2number,  zernike_short_name, parse_zernikes
from ..misc.parser import asarray
from ..header import get_header, set_header
from ..pupil.base import Pupil
from .phase_screen import ModePhaseScreen, _wr_modes, _rd_modes

class Zernike(Data):
    """ a class handling a zernike (int or (n,m) tuple) as data """
    pupil = None
    def __init__(self, data, header):
        data = parse_zernikes(data)
        super(Zernike, self).__init__(data, header)
    
    @property
    def number(self):
        return zernike_polynom2number(self.data)
        
    @property
    def polynom(self):
        return self.data        
        
    def phase_screen(self, pupil=None, orientation=None, angle=None):
        pupil = self.pupil if pupil is None else pupil
        if pupil is None:
            pupil = Pupil.from_header(self.header)           
        orientation = orientation or get_header(self.header, "ALIGNMENT.ORIENTATION", 'xy')
        angle = get_header(self.header, "ALIGNMENT.ANGLE", 0.0) if angle is None else angle
        ztp = theoritical_phase(self.polynom, pupil, orientation=orientation, angle=angle)
        if len(ztp.shape) > 2:
            h = {}
            _wr_modes(h, self.polynom)
            return ModePhaseScreens(ztp)
        else:
            h = {}
            h['ZERNIKE'] = self.number
            return ModePhaseScreen(ztp)
    
    def info(self):
        znum = self.number
        return {'z': self.number, 'name':zernike_short_name(self.polynom), 'pol':self.polynom}

class Zernikes(Zernike):
    def __iter__(self):
        h = self.header.copy()
        for z in self.polynom:
            yield Zernike(z, h.copy())
    

        
class Mode(Zernike):
    def __init__(self, data, header):
        try:
            get_header(header, "ZERNIKE")
        except KeyError:
            raise ValueError("Zernike number is missing in header")
        Data.__init__(self, data, header)
    
    @property
    def number(self):
        return zernike_polynom2number(parse_zernikes(get_header(self.header, 'ZERNIKE')))
        
    @property
    def polynom(self):
        return parse_zernikes(get_header(self.header, 'ZERNIKE'))
    
    def phase(self, pupil=None, orientation='xy', angle=0.0):
        return super(ZernikeAmplitude).phase(pupil=pupil, orientation=orientation, angle=angle)*self.data
    

class Modes(Mode):
    def __init__(self, data, header):
        #data = parse_zernikes(data)
        Data.__init__(self, data, header)
        #super(Modes, self).__init__(data, header)
    
    @property
    def polynom(self):
        return _rd_modes(self.header)
    
    @property
    def number(self):
        return zernike_number2polynom(self.polynom)
    
    def __iter__(self):
        h = self.header.copy()
        
        for z,n in zip(self.data, self.number):
            ih = h.copy()
            ih['ZERNIKE'] = n
            yield Mode(z, ih)
    

    
    