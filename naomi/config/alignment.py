from .base import Config
from ..header import get_header, set_header
from ..compute.command import ifm_scale, if_center
from ..misc.parser import asdm


class Alignment(Config):
    """ Represent the DM to wavefront alignment
    
    Typicaly created from a ifm: 
       `Alignment.from_ifm()` -> pixel scale and centering computed
                                from the interaction function matrix
        `Alignment.from_header` -> from a header containing alignment keywords                   
    Or by hand: 
        `Alignment([64.0, 64.0], [0.38e-3, 0.38e-3], 0.0)`
    
    Properties
    ----------
    center : (2,) float array like [pixel]
        x,y position of the DM center as seen by the wfs
    scale : (2,) float array like [m/pixel]
        x,y scale of the DM Pupil as seen by the wfs
    angle : float [rad]
        rotation angle of DM as seen by the wfs
        
    Inputs
    ------
    center
    scale
    angle (optional), default is 0.0        
    """
    # center of the dm seen by Wfs
    center = (0.0, 0.0)
    # scale in m/pixel of the wfs
    scale  = (1.0, 1.0)
    # angle of the dm seen by wfs
    angle  = 0.0
        
    def __init__(self, center=None, scale=None, angle=0.0, **kwargs):
        Config.__init__(self, **kwargs)    
        if center is None:
            raise ValueError("center cannot be None")
        if scale is None:
            raise ValueError("scale cannot be None")
        
        self.center = center
        self.scale = scale
        self.angle = angle
        
    @classmethod
    def from_header(cl, h):
        """ create an alignment object from a header
        
        Inputs
        ------
        h: dict like object 
            only requirement is the getitem `value = h[key]` method
            it must raise a KeyError if Key not found 
        """
        
        center = [get_header(h, k) for k in ['ALIGNMENT.XCENTER', 'ALIGNMENT.YCENTER']]
        scale  = [get_header(h, k) for k in ['ALIGNMENT.XPSCALE', 'ALIGNMENT.YPSCALE']]
                
        angle = get_header(h, 'ALIGNMENT.ANGLE', 0.0)         
        return cl(center, scale, angle)
    
    @classmethod
    def from_ifm(cl, ifm, dm=None): 
        """ create an alignment object from an ifm 
        
        A dm object must be provided to compute scale 
        
        Inputs
        ------
        ifm : (N, Nx, Ny) array like or data object
        dm :  :class:`naomi.config.DM`
            dm configuration object or string or None (default)
        """     
        dm = asdm(dm)                                  
        center = if_center(ifm[dm.centralact])
        scale = ifm_scale(ifm, dm)
        # :TODO: measure the angle from ifm  
        angle = 0.0
        return cl(center, scale, angle)
    
    def to_header(self, h=None):
        h = dict() if h is None else h
        set_header(h, 'ALIGNMENT.XCENTER', self.center[0])
        set_header(h, 'ALIGNMENT.YCENTER', self.center[1])   
        
        set_header(h, 'ALIGNMENT.XPSCALE', self.scale[0])
        set_header(h, 'ALIGNMENT.YPSCALE', self.scale[1])
        
        set_header(h, 'ALIGNMENT.ANGLE', self.angle)
        return h
    
         
            