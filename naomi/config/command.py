from ..misc.parser import aspupil
from ..pupil.base import Pupil
from ..header import get_header, set_header
from .base import Config
 
class Command(Config):
    def __init__(self, pupil, orientation='xy', angle=0.0):
        self.pupil = aspupil(pupil)
        self.orientation = orientation 
        self.angle = angle
        
    @classmethod
    def from_header(cl, h):
        pupil =  Pupil.from_header(h)
        orientation = get_header('ALIGNMENT.ORIENTATION', 'xy')
        angle = get_header('ALIGNMENT.ANGLE', 0.0)
        return cl(pupil, orientation=orientation, angle=angle)
    