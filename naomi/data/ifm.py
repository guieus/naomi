from .base import Data

from ..misc.parser import asdm
from ..compute.command import if_center, ifm_scale, command_matrices, clean_ifm
from ..fitting import fit_if_profile, if_profile, img_if_profile

class IFM(Data):
    environment = None
    
    @classmethod
    def from_hdulist(cl, hdulist):
        data = hdulist[0].data
        header = hdulist[0].header
        
        for hdu in hdulist:
            if hdu.name == "ENVIRONMENT":
                # :TODO: do something with environmne data
                break
     
    def center(self, dm=None):
        dm = asdm(dm)
        return if_center(self.data[dm.centralact])
    
    def scale(self, dm=None):
        dm = asdm(dm)
        return ifm_scale(self.data, dm)
    
    def command_matrices(self, pupil, zernikes, orientation='xy', angle=0.0, 
                         nEigenValue=None, zeromean=False):        
        return command_matrices(self.data, pupil, zernikes, 
                    orientation = orientation, 
                    angle = angle, 
                    nEigenValue = nEigenValue, 
                    zeromean = zeromean
                )
    
    def cleaned(self, rad, prc=50, cleanTipTilt=False):
        data = clean_ifm(rad, prc, cleanTipTilt=cleanTipTilt)
        header = self.header.copy()
        cleaned = self.__class__(data, header)
        
        cleaned.set_key('IFM CL PERCENTIL', prc)
        cleaned.set_key('IFM CL RADIUS', rad)
        cleaned.set_key('IFM CL TIPTILT', cleanTipTilt)
        cleaned.set_key('IFM CLEANED', True)
        return cleaned
    
    def profiles(self, ftype='naomi', sigma_guess=10, method='dogbox'):
        return [fit_if_profile(i, ftype, sigma_guess=sigma_guess, method=method) for i in self.data]
    
    
    
    
    
    
    
                
            
        