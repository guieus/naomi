from ..config.base import Config
from ..header import get_header, set_header
from ..misc.registers import pupil_register


class Pupil(Config):        
    type = ""
    
    @staticmethod 
    def from_header(h, suffix="", __fmh__=False):
        if __fmh__:
            raise RuntimeError("BUG Pupil subclass does not define a from_header method")
        try:
            ptype = get_header(h, suffix+"Pupil.TYPE")
        except KeyError:
            raise ValueError("Cannot find %s information"%(suffix+"Pupil.TYPE"))                
        return pupil_register.get_class(ptype).from_header(h, suffix, __fmh__=False)        
                                
class PupilInstance(Config):    
    def __init__(self, mask, size, center, angle=0.0, builder=None):
        self.mask = mask
        self.size = size
        self.center = center
        self.angle = angle
        self.builder = builder
        
    @property
    def shape(self):
        return self.mask.shape

def parse_pupil(pupil):
    return pupil
    