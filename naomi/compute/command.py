import numpy as np 
from ..misc.parser import asarray,  aspupil, asdm
from ..misc.mesh   import oriented_polar, cart2pol
from ..pupil.base import PupilInstance
from .phase import zernfun, parse_zernikes

def invert_ifm(ifm, nEigenValue=None, zeromean=False, mask=None):
    """ ifm matrix invertion  
    
    Inputs
    ------
    ifm : (N, Ny, Nx) array like 
          Influence Function matrix
    nEigenValue : int, optional
          Number of Eigen value to keep 
    zeromean :  bool, optional
                If True the mean of all actuator is forced to be 0
                default is False
    mask : (Ny, Nx) array like 
        Apply the IFM matrix inversion only on a masked region
        
    output
    ------
    PtC : (Ny, Nx, N) array 
          The Phase to command matrix 
    """
    ifm = asarray(ifm)
    if mask is not None:
        mask = asarray(mask)
        ifm = ifm * mask
    else:
        ifm = ifm.copy()            
    ifm[np.isnan(ifm)] = 0.0
    nAct, nSubY, nSubX = ifm.shape    
    PtC = pseudo_invert(ifm.reshape(nAct, nSubY*nSubX), nEigenValue=nEigenValue, zeromean=zeromean, mask=mask)
    
    
    return PtC.reshape(nSubY, nSubX, nAct)

def pseudo_invert(M, nEigenValue, zeromean=False, mask=None): 
    """ Invert a matrix of images like IFM     
    
    Inputs
    ------
    M : (N, M) array like 
        N, M matrix to be inverted 
    nEigenValue : int, optional
          Number of Eigen value to keep 
    zeromean :  bool, optional
                If True the mean of all second dimension of the inverted matrix is 
                force to be 0. Default is False
    
    Outputs
    -------
    Mi : (M,N) array 
          Inverted matrix 
    """
    n1, n2 = M.shape    
    
    CtP = np.zeros([ n1, n2+1])
    CtP[:,0:-1] = M
    CtP[np.isnan(CtP)] = 0.0
    CtP[:,-1] = zeromean * 1000
    
    [U,s,Vh] = np.linalg.svd(CtP, full_matrices=False)
    V = Vh.T
    # Filter Eigen values
    if nEigenValue is not None:
        r1 = nEigenValue-1;
        V = V[:,:r1]
        U = U[:,:r1]
        s = s[:r1]
    
    # inverse of the diagonal matrix is the     
    si = 1./s;
    
    PtC = np.matmul((V*si), U.T) 
    #PtC = np.matmul( np.matmul(V, np.diag(1/s).T), U.T) 
    PtC = PtC[:-1,:]   
    return PtC


def command_matrices(ifm,  pupil, zernikes, orientation='xy', angle=0.0, 
                     nEigenValue=None, zeromean=False, dm=None, invertztp=False): 
    """ compute the command matrices from a ifm and a Pupil definition 
    
    Inputs
    ------
    ifm : (N, Ny, Nx) array like 
          Influence function matrix 
    pupil : a :class:`naomi.PupilInstance` or a :class:`naomi.Pupil object 
         represent the location of the Pupil within the Ny,Nx image.
    zernikes : list of zernike number or poynomials (2 tuple)            
          
    orientation: string, optional
        see :meth:`naomi.misc.mesh.oriented_polar`
    angle : float, optional 
        offset angle (on top of what is defined in orientation)
    nEigenValue : int, optional
          Number of Eigen value to keep 
    zeromean :  bool, optional
                If True the mean of all actuator is forced to be 0
                default is False   
    dm : (optional) :class:`naomi.config.DM`
        dm configuration object or string or None (default)
        This is only used if Pupil is a Pupil object and not a PupilInstance
    invertztp: (bool, optional) if true the ptz (invert of ztp) is returned      
    
    Outputs
    ------- 
    ptc : (Ny, Nx, N) 
          Invert of ifm, the phase to command matrix 
    ztc : (N, Nact)
    ztp : (N, Ny, Nx)
    ptz : (Ny, Nx, N) if invertztp=True
    """
    
    zernikes = asarray(parse_zernikes(zernikes))
    
    ifm = asarray(ifm)
    
    if not isinstance(pupil, PupilInstance):        
        pupil = pupil.make_from_ifm(ifm, dm)                
    
     
    ptc = invert_ifm(ifm, nEigenValue, zeromean=zeromean, mask=pupil.mask)
    if invertztp:
        ztp, ptz = theoritical_phase(zernikes, pupil, orientation=orientation, angle=angle, invert=True)
    else:
        ztp = theoritical_phase(zernikes, pupil, orientation=orientation, angle=angle)
                
    nAct, nY, nX = ifm.shape
    nZernike, _ = zernikes.shape
    
    ztpi = ztp.copy()
    ztpi[np.isnan(ztp)] = 0.0
    
    ztc = np.matmul(ztpi.reshape(nZernike,nY*nX), ptc.reshape(nY*nX, nAct))   
    if invertztp:
        return ptc,ztc,ztp,ptz
    return ptc,ztc,ztp
        
    
def _theoriticalPhase(zernike_polynom, r, theta, mask):   
    #n,m = noll_to_zern(zernike)
    n,m = zernike_polynom
    z = zernfun(n,m,r[mask],theta[mask])
    z = z / nanrms(z)
    phaseArray = np.ones(mask.shape)*np.nan;
    phaseArray[mask] = z 
    return phaseArray

def invert_phaseCube(ZtP):
    """ Invert a Zernike to Phase matrix 
    
    Inputs
    ------
    ZtP : (N, Nx, Ny) zernike to Phase Matrix 
    
    Outputs
    -------
    PtZ : (Nx, Ny, N) Phase to Zernike inversed matrix 
    """
    #PtZArray = reshape(pinv(reshape(naomi.compute.nanzero(ZtPArray),nZernike,[])),nSubAperture,nSubAperture,[]);
    ZtP = asarray(ZtP).copy()
    
                
    ZtP[np.isnan(ZtP)] = 0.0
    nMode, nSubY, nSubX = ZtP.shape    
    PtZ = pseudo_invert(ZtP.reshape(nMode, nSubY*nSubX), None, zeromean=False, mask=None)
    return PtZ.reshape(nSubY, nSubX, nMode)
    

def theoritical_phase(zernikes, pupil, orientation='xy', angle=0.0, invert=False):
    """ Make theoritical pure zernikes phase screen 
    
    Inputs
    ------
    zernikes : a int for zernike number, a 2 tuple for zernike polynoms or 
              an iterable containing zernike number or zernike polynoms
              In this case ouput number of phase will be N=len(zernikes)     
    pupil : a :class:`naomi.PupilInstance` object 
         represent the location of the Pupil within a Ny,Nx image
         the Pupil object also gives the centering and the typical size in each 
         dimensions.
    orientation: string, optional
        see :meth:`naomi.misc.oriented_polar`
    angle : float, optional 
        offset angle (on top of what is defined in orientation)
    invert: bool, opiotnal
        invert the matrix to return  also the Phase2Mode.
        if invert is True, zernikes must be an array.  
    Ouputs
    ------
    ZtP: (Nx, Ny) or (N, Nx, Ny) depending of `zernike` input dimension
        Cube of pure zernike phase screen  
    
    PtZ: **only returned if invert=True** (Nx, Ny, N) if invert is True
    
    Example
    -------
        from naomi  import Disk , theoritical_phase
        from matplotlib.pylab import plt
        
        # a disk inside a [128,128] image centered at pixel [63,63]
        p = Disk(70,'pix').make([128,128], [63,63])
        
        plt.imshow(theoritical_phase(4,p) )
    """
    pupil = aspupil(pupil)        
    msize = pupil.size
    mcenter =  pupil.center
    mask = pupil.mask.copy()
    
    nY, nX =  mask.shape
    [X, Y] = np.meshgrid(range(nX), range(nY));
        
    r, theta = cart2pol((X-mcenter[0])/(msize[0]/2.0) , (Y-mcenter[1])/(msize[1]/2.0))    
    theta = oriented_polar(theta, orientation, angle)
    
    mask[r>1.0] = False    
    mask[r<0.0] = False    
    
    
    zernikes = np.asarray(parse_zernikes(zernikes))    
        
    if zernikes.ndim>1:
        ny, nx = mask.shape
        nPhase, nm = zernikes.shape
        if nm!=2:
            raise ValueError('zernikes must be a 2xtuple a list of 2 tuple or a (N,2) array got a shape of %r'%(zernikes.shape,))
        phaseCube = np.ndarray([nPhase, ny, nx])
        for i, zern in enumerate(zernikes):
            phaseCube[i,...] = _theoriticalPhase(zern, r, theta, mask)
        if invert:
            return phaseCube, invert_phaseCube(phaseCube)
            
        return phaseCube
    else:
        if invert:
            raise ValueError('can only invert the matrix if zernike is an array')
        if len(zernikes)!=2:
            raise ValueError('zernikes must be a 2xtuple a list of 2 tuple or a (N,2) array got a shape of %r'%zernikes.shape)        
        return _theoriticalPhase(zernikes, r, theta, mask) 


def zernike_vector(phase, ptz):
    """ Make a zernike vector measured from a phase
    
    Inputs
    ------
    phase: (Nx,Ny) a phase screen     
    ptz : (Nzernike, Nx, Ny) a theoritical phase to zernike matrix 
        as created by theoritical_phase(...,invert=True)
    
    Ouputs
    ------
    zernikes: (Nzernike,) vector of measured mode amplitude
    """
    phase = asarray(phase).copy()
    
    phase[np.isnan(phase)] = 0.0
    ptz = asarray(ptz)
    nX, nY, nMode = ptz.shape
    ptz = ptz.reshape(-1,nMode)
    return np.matmul(phase.reshape(nX*nY), ptz)
    # naomi.compute.nanzero(obj.phaseArray(':')') * reshape(PtZ,nSubAperture*nSubAperture,[]);

def phase_from_zernikes(zernikes, ztp):
    """ compute a phase from a vector of zernikes and a ztp matrix
    
    Inputs
    -----
    zernikex : (Nz,) zernike modes amplitudes 
    ztp : (Nz, Ny, Nx) Zernike to Phase matrix
    
    Outputs
    -------
    phase :(Ny, Nx) phase screen 
    """
    zernikes = asarray(zernikes)
    ztp = asarray(ztp)
    
    nZern1, nX, nY = ztp.shape
    nZern, = zernikes.shape
    if nZern>nZern1:
        raise ValueError("Not enough mode in the ztp matrix")
    ztp = ztp[0:nZern,:,:]
    return (ztp.reshape(nZern, nX*nY).T*zernikes).sum(axis=1).reshape(nY,nX)

def clean_tiptilt(phase, on=None):
    nY, nX =  phase.shape
    X, Y = np.meshgrid(range(nX), range(nY));
    xdelta = np.diff(phase,axis=1)
    ydelta = np.diff(phase,axis=0)
    if on is None:
        on = phase
    newPhase = on   - (X-nX/2.0) * np.nanmedian(xdelta)
    newPhase = newPhase - (Y-nY/2.0) * np.nanmedian(ydelta)
    return newPhase
        
def clean_if(ctp, rad, prc=50, cleanTipTilt=False, XY=None):
    """ clean a Influence Function from piston and tiptilt 
    
    Inputs
    ------
    ctp : (nY, nX) array like
        Influance function phase screen
    rad : float  [pixel]
        Radius of the circle of exclusion to compute piston and tiptilt
    prc : float, optional  [%]
        Floor percentil for piston measurement (e.i. 50% is the median)
        
    Outputs
    -------
    ctp_cleaned : (nY, nX) array 
        cleaned ctp
    """
    ctp = asarray(ctp)
    
    if XY is None:
        nY, nX = ctp.shape
        X,Y = np.meshgrid(range(nX), range(nY))
    else:
        X,Y = XY
    
    # remove piston 
    ctp = ctp - np.nanmedian(ctp)
    # discard pixels around the actuator
    max = np.nanmax(np.abs(ctp))
    yA,xA = (o[0] for o in  np.where(np.abs(ctp)==max))

    maska = (X-xA)**2 + (Y-yA)**2 < (rad*rad);
    ctp_f = ctp.copy();
    ctp_f[maska] = np.nan;
    if cleanTipTilt:
        ctp   = clean_tiptilt(ctp_f, ctp)
        ctp_f = clean_tiptilt(ctp_f)
        
      
    # % Clean tip-tilt
    # if cleanTipTilt
    #     xdelta = diff(ctp_f');
    #     ctp = ctp - (X-nSubAperture/2) * median(xdelta(~isnan(xdelta)));
    #     ydelta = diff(ctp_f);
    #     ctp = ctp - (Y-nSubAperture/2) * median(ydelta(~isnan(ydelta)));

    # Clean piston with percentil

    ctp_fs = np.sort( np.abs(ctp_f[~np.isnan(ctp_f)]))      
    idx = int(prc/100.0*ctp_fs.size);
    piston = ctp_fs[idx];      
    ctp = ctp - piston         
    return ctp
    
def clean_ifm(ifm, rad, prc, cleanTipTilt=False):  
    """ clean ifm from piston and eventualy tiptilt """  
    ifmc = np.ndarray(ifm.shape)
    nActuator, nY, nX = ifm.shape
    
    XY = np.meshgrid(range(nX), range(nY))
    
    for iAct in range(nActuator):
        ctp = ifm[iAct,:,:]
        ifmc[iAct,:,:] = clean_if(ctp, rad, prc, cleanTipTilt=cleanTipTilt, XY=XY)                
    return ifmc

def if_center(ifArray)  :
    """ compute the center of an influence function """
    nX, nY = ifArray.shape
    # Clean the IF
    #ifArray = np.ma.masked_array(ifArray, np.isnan(ifArray))
    ifCleanArray = np.abs(ifArray - np.nanmedian(ifArray));
    #ifCleanArray.mask += ifCleanArray<(0.1*np.nanmax(ifCleanArray))
    ifCleanArray[ifCleanArray<(0.1*np.nanmax(ifCleanArray))] = np.nan;
    ifCleanArray = ifCleanArray - np.nanmin(ifCleanArray);
        
    # Compute barycenter
    X,Y = np.meshgrid(range(nX),range(nY));
    C  = np.nansum(ifCleanArray);
    xC = np.nansum(ifCleanArray * X) / C
    yC = np.nansum(ifCleanArray * Y) / C
    return xC, yC

def ifm_scale(ifm, dm):
    """ from an IFM compute the scale with the actsep unit/pixel
    
    Inputs
    ------
    ifm : (N, Nx, Ny) 
        Influence function matrix 
    dm: a dm.Dm object or dm name         
    """
    
    dm = asdm(dm)
        
    N = 2048*4;

    # Compute scaling in m/pix, assuming
    # 5mm period in the above signals
    # Discard the 100-first frequencies
    scales = []
    for i,axis,sep in [[dm.i, 2, dm.actuator_separation[0]], 
                       [dm.j, 1, dm.actuator_separation[1]]]:
        array = np.nansum(ifm,axis=axis)
        
        vector = (np.sum(array[(i%2)==0,:],axis=0) - np.sum(array[(i%2)>0,:],axis=0))
        fftVector = np.abs(np.fft.fft(vector[~np.isnan(vector)],N))
        fftVector[0:100] = 0.0
        xf = np.argmax(fftVector[0:N//2])+1
        scales.append(sep*2 / (N/xf))        
    return scales
    
def reconstruct_phase(cmd, ifm):
    """ Reconstruct a phase scree from dm command and an ifm 
    
    Inputs
    ------
    cmd: (Nact,) vector of DM commands
    ifm: (Nact, Ny, Nx) Influance function matrix 
    """
    nAct, nSubY, nSubX = ifm.shape
    phase = np.matmul(cmd, ifm.reshape(nAct, nSubY*nSubX))
    phase = phase.reshape(nSubY,nSubX)
    return phase


def restric_phase(phase, pupil):
    """ return a flat vector of subPupil contained inside a Pupil 
    
    Inputs
    ------
    phase : (Ny, Nx) array like
    Pupil : a :class:`naomi.PupilInstance` or a :class:`naomi.Pupil object 
         represent the location of the Pupil within the Ny,Nx image.
    
    Outputs
    -------
    vector : (N,) array 
        N will be np.sum(pupil.mask)    
    """
    Pupil = aspupil(pupil)
    phase  = asarray(phase)
    return phase[..., pupil.mask]

def mask_phase(phase, pupil):
    """ Fill a phase screen array of nan for subPupil outside of the pupuill
    
    Inputs
    ------
    phase : (Ny, Nx) array like
    pupil : a :class:`naomi.PupilInstance` or a :class:`naomi.Pupil object 
         represent the location of the Pupil within the Ny,Nx image.
    
    Outputs
    -------
    phase : (Ny, Nx) array
    """
    phase  = asarray(phase).copy()
    phase[~pupil.mask] = np.nan
    return phase

def nanrms(x, axis=None):
    return np.sqrt(np.nanmean(x**2, axis=axis))
def rms_pupil(phase, pupil):
    """ phase rms on the given pupil 
    
    Inputs
    ------
    phase : (Ny, Nx) array like
    pupil : a :class:`naomi.pupilInstance` or a :class:`naomi.pupil object 
         represent the location of the pupil within the Ny,Nx image.
    
    Outputs
    -------
    rms : float
    """
    return np.nanrms(restric_phase(phase, pupil))

def mean_pupil(phase, pupil):
    """ phase mean on the given pupil 
    
    Inputs
    ------
    phase : (Ny, Nx) array like
    pupil : a :class:`naomi.PupilInstance` or a :class:`naomi.pupil object 
         represent the location of the pupil within the Ny,Nx image.
    
    Outputs
    -------
    mean : float
    """
    return np.nanmean(restric_phase(phase, pupil))

def median_pupil(phase, pupil):
    """ phase median on the given pupil 
    
    Inputs
    ------
    phase : (Ny, Nx) array like
    pupil : a :class:`naomi.PupilInstance` or a :class:`naomi.pupil object 
         represent the location of the pupil within the Ny,Nx image.
    
    Outputs
    -------
    median : float
    """
    return np.nanmedian(restric_phase(phase, pupil))